/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const uuid = require('node-uuid');
const Joi = require('joi');
const Controllers = require('../Controllers');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const Config = require('../Config');


const customerRegister = {
    method: 'POST',
    path: '/api/customer/register',
    handler: (request, reply) => {
        let payload = request.payload;
        Controllers.CustomerCtrl.customerRegister(payload, (err, data) => {
            if (err) {
                reply(UniversalFunctions.sendError(err));
            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.CREATED, data)).code(201);
            }
        });
    },
    config: {
        description: 'Register Customer',
        tags: ['api', 'customer'],
        /*   payload: {
         maxBytes: 2000000,
         output: 'file',
         parse: true
         },*/
        validate: {
            payload: {
                fullName: Joi.string().regex(/^[a-zA-Z ]+$/).min(2).required(),
                email: Joi.string().required().email(),
                password: Joi.string().required().min(6),
                //countryCode: Joi.string().required(),
                mobileNumber: Joi.string().min(5).regex(/^[0-9]+$/).required(),
                deviceToken: Joi.string().required(),
                referralCode: Joi.string().optional().allow(''),
                deviceType: Joi.string().required().valid([Config.AppConstants.DATABASE.DEVICETYPE.IOS, Config.AppConstants.DATABASE.DEVICETYPE.ANDROID]),
                gender: Joi.string().required().valid([Config.AppConstants.DATABASE.GENDER.MALE, Config.AppConstants.DATABASE.GENDER.FEMALE]),
                /*     profilePic: Joi.any()
                 .meta({swaggerType: 'file'})
                 .optional()
                 .description('image file')*/
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                //payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const customerLogin = {
    method: 'POST',
    path: '/api/customer/login',
    handler: function (request, reply) {
        let payload = request.payload;
        Controllers.CustomerCtrl.customerLogin(payload, (err, data) => {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Login for Customer',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                emailOrPhone: Joi.string().required(),
                password: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const customerLogout = {
    method: 'PUT',
    path: '/api/customer/logout',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerCtrl.customerLogout(userData, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Customer Logout',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const getProfile = {
    method: 'GET',
    path: '/api/customer/getProfile',
    config: {
        description: 'Get profile of Customer',
        auth: 'UserAuth',
        tags: ['api', 'customer'],
        handler: function (request, reply) {
            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            console.log("-----route------>>" + JSON.stringify(userData));
            if (userData && userData.id) {
                Controllers.CustomerCtrl.getProfile(userData, function (err, success) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                        //reply(err);
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, success));
                        //reply(userData);
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(Config.AppConstants.STATUS_MSG.ERROR.INVALID_TOKEN));
                //reply("Invalid Token");
            }
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const verifyOTP = {
    method: 'PUT',
    path: '/api/customer/verifyOTP',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerCtrl.verifyOTP(request.payload, userData, (err, data) => {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.VERIFY_COMPLETE, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Verify OTP for Customer',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                OTPCode: Joi.string().length(4).required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const resendOTP = {
    method: 'PUT',
    path: '/api/customer/resendOTP',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerCtrl.resendOTP(userData, (err, data) => {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Resend OTP for Customer',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

let changePassword = {
    method: 'PUT',
    path: '/api/customer/changePassword',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerCtrl.changePassword(request.payload, userData, (err, data) => {
            if (!err) {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.PASSWORD_RESET, data));
                //return reply("Password Changed Successfully", data);
            }
            else {
                reply(UniversalFunctions.sendError(err));
                //return reply(err);
            }
        });
    },
    config: {
        description: 'Change Password',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                oldPassword: Joi.string().required().min(6),
                newPassword: Joi.string().required().min(6)
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var forgotPassword = {

    method: 'POST',
    path: '/api/customer/forgetPassword',
    config: {
        description: 'Forgot Password',
        tags: ['api', 'customer'],
        handler: function (request, reply) {
            /*     var data = {
             mobileNumber: request.payload.mobileNumber
             };*/
            var data = request.payload;
            Controllers.CustomerCtrl.forgetPassword(data, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.NEW_PASSWORD, success));
                }
            });
            //}
        },
        validate: {
            payload: {
                emailOrPhone: Joi.string().trim().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }

    }
};

const getAllSp = {
    method: 'GET',
    path: '/api/customer/getAllSp',
    config: {
        description: 'Get All Service Providers',
        tags: ['api', 'customer'],
        handler: function (request, reply) {

            Controllers.CustomerCtrl.getAllSp((err, success) => {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                    //reply(err);
                } else {
                    reply(UniversalFunctions.sendSuccess(null, success));
                    //reply(userData);
                }
            });

        },
        validate: {
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const getAllBookings = {
    method: 'GET',
    path: '/api/customer/getAllBookings',
    config: {
        description: 'Get All Bookings',
        auth: 'UserAuth',
        tags: ['api', 'customer'],
        handler: (request, reply) => {
            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;


            Controllers.CustomerCtrl.getAllBooking(userData, function (err, success) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                    //reply(err);
                } else {
                    reply(UniversalFunctions.sendSuccess(null, success));
                    //reply(userData);
                }
            });


        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const rebookSession = {
    method: 'POST',
    path: '/api/customer/rebook',
    handler: (request, reply) => {
        let userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerCtrl.rebookSession(request.payload, userData, (err, user) => {
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, user));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Rebook by Customer',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                bookingId: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const accessTokenLogin = {
    method: 'POST',
    path: '/api/customer/accessTokenLogin',
    handler: (request, reply) => {
        let userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        if (userData) {
            return reply(UniversalFunctions.sendSuccess(null, userData));
        }
        else {
            return reply(UniversalFunctions.sendError(null));
        }
    },
    config: {
        description: 'Access Token Login',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};


module.exports = [
    customerRegister,
    customerLogin,
    customerLogout,
    getProfile,
    verifyOTP,
    resendOTP,
    changePassword,
    forgotPassword,
    getAllSp,
    getAllBookings,
    rebookSession,
    accessTokenLogin
];