/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const uuid = require('node-uuid');
const Joi = require('joi');
const Controllers = require('../Controllers');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const Config = require('../Config');
const booking = Config.AppConstants.DATABASE.BOOKING_STATUS;

let spRegister = {
    method: 'POST',
    path: '/api/sp/register',
    handler: (request, reply) => {
        let payload = request.payload;
        Controllers.SpCtrl.spRegister(payload, (err, data) => {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.CREATED, data)).code(201);
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Register Service Provider',
        tags: ['api', 'sp'],
        validate: {
            payload: {
                fullName: Joi.string().regex(/^[a-zA-Z ]+$/).min(2).required(),
                email: Joi.string().required().email(),
                password: Joi.string().required().min(6),
                //countryCode: Joi.string().required(),
                mobileNumber: Joi.string().min(5).regex(/^[0-9]+$/).required(),
                deviceToken: Joi.string().required(),
                deviceType: Joi.string().required().valid([Config.AppConstants.DATABASE.DEVICETYPE.IOS, Config.AppConstants.DATABASE.DEVICETYPE.ANDROID])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                //payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

let spUpdateLocation = {
    method: 'PUT',
    path: '/api/sp/updateLocation',
    handler: (request, reply) => {
        let userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.SpCtrl.spUpdateLocation(request.payload, userData, (err, user) => {
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, user));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Update Location',
        tags: ['api', 'sp'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                latitude: Joi.number().required().min(-90).max(90),
                longitude: Joi.number().required().min(-180).max(180)
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

/*const updateSpAvailability = {
 method: 'POST',
 path: '/api/sp/updateAvailability',
 config: {
 description: 'Sp updates availabilty',
 tags: ['api', 'sp'],
 auth: 'UserAuth',
 handler: (request, reply) => {
 let userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
 //if ((userData && userData._id) && userData.userType === CONSTANT.USER_TYPE.SERVICE_PROVIDER) {
 Controllers.SpCtrl.addAndUpdateSpAvailability(userData, request.payload, (error, success) => {
 if (error) {
 //console.log("error " + JSON.stringify(error));
 return reply(UniversalFunctions.sendError(error));
 } else {
 //console.log("success " + success);
 return reply(UniversalFunctions.sendSuccess(null, success));
 }

 });
 //}
 /!* else {
 reply(util.sendError(CONSTANT.STATUS_MSG.ERROR.INVALID_TOKEN));
 }*!/
 },
 validate: {
 headers: UniversalFunctions.authorizationHeaderObj,
 payload: {
 date: Joi.string().required(),
 data: Joi.array().items(Joi.object().keys({
 //serviceAvailabilityId: Joi.string().required(),
 time: Joi.number().required(),
 isAvailable: Joi.boolean()
 })).required()
 },
 failAction: UniversalFunctions.failActionFunction
 },
 plugins: {
 'hapi-swagger': {
 //payloadType: 'json',
 responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
 }
 }
 }
 };*/

const updateBookingStatus = {
    method: 'POST',
    path: '/api/v1/sp/updateBooking',
    handler: (request, reply) => {
        let userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.SpCtrl.updateBookingStatus(request.payload, userData, (err, user) => {
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, user));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Update Booking Status by SP',
        tags: ['api', 'sp'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                bookingId: Joi.string().required(),
                bookingStatus: Joi.string().required().valid([booking.ACCEPTED, booking.DECLINED, booking.REACHED, booking.STARTED, booking.COMPLETED, booking.CANCELLED])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};


const spLogin = {
    method: 'POST',
    path: '/api/sp/login',
    handler: (request, reply) => {

        Controllers.SpCtrl.spLogin(request.payload, (err, data) => {
            if (err) {
                reply(UniversalFunctions.sendError(err));

            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
            }
        });
    },
    config: {
        description: 'Login for Washer',
        tags: ['api', 'sp'],
        validate: {
            payload: {
                emailOrPhone: Joi.string().required(),
                password: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const spLogout = {
    method: 'PUT',
    path: '/api/sp/logout',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.SpCtrl.spLogout(userData, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));

            } else {
                reply(UniversalFunctions.sendSuccess(null, data));

            }
        });
    },
    config: {
        description: 'Washer Logout',
        tags: ['api', 'sp'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const getProfile = {
    method: 'GET',
    path: '/api/customer/getProfile',
    config: {
        description: 'Get Profile of Washer',
        auth: 'UserAuth',
        tags: ['api', 'sp'],
        handler: (request, reply) => {
            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            console.log("-----route------>>" + JSON.stringify(userData));
            if (userData && userData.id) {
                Controllers.SpCtrl.spGetProfile(userData, function (err, success) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, success));
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(Config.AppConstants.STATUS_MSG.ERROR.INVALID_TOKEN));
            }
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const spVerifyOTP = {
    method: 'PUT',
    path: '/api/sp/verifyOTP',
    handler: (request, reply) => {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.SpCtrl.spVerifyOTP(request.payload, userData, (err, data) => {
            if (err) {
                reply(UniversalFunctions.sendError(err));

            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.VERIFY_COMPLETE, data));
            }
        });
    },
    config: {
        description: 'Verify OTP for Washer',
        tags: ['api', 'sp'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                OTPCode: Joi.string().length(4).required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const spResendOTP = {
    method: 'PUT',
    path: '/api/sp/resendOTP',
    handler: (request, reply) => {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.SpCtrl.spResendOTP(userData, (err, data) => {
            if (err) {
                reply(UniversalFunctions.sendError(err));

            } else {
                reply(UniversalFunctions.sendSuccess(null, data));

            }
        });
    },
    config: {
        description: 'Resend OTP for Washer',
        tags: ['api', 'sp'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

let spChangePassword = {
    method: 'PUT',
    path: '/api/sp/changePassword',
    handler: (request, reply) => {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.SpCtrl.spChangePassword(request.payload, userData, (err, data) => {
            if (!err) {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.PASSWORD_RESET, data));
            }
            else {
                reply(UniversalFunctions.sendError(err));

            }
        });
    },
    config: {
        description: 'Change Password for Washer',
        tags: ['api', 'sp'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                oldPassword: Joi.string().required().min(6),
                newPassword: Joi.string().required().min(6)
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var spForgotPassword = {

    method: 'POST',
    path: '/api/sp/forgetPassword',
    config: {
        description: 'Forgot Password for Washer',
        tags: ['api', 'sp'],
        handler: (request, reply) => {
            /*     var data = {
             mobileNumber: request.payload.mobileNumber
             };*/
            var data = request.payload;
            Controllers.SpCtrl.spForgetPassword(data, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.NEW_PASSWORD, success));
                }
            });
            //}
        },
        validate: {
            payload: {
                mobileNumber: Joi.string().trim().required().min(10)
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }

    }
};

module.exports = [
    spRegister,
    spUpdateLocation,
    updateBookingStatus,
    spLogin,
    spLogout,
    spResendOTP,
    spVerifyOTP,
    spChangePassword,
    spForgotPassword
];