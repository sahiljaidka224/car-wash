/**
 * Created by cl-macmini-88 on 8/24/16.
 */

'use strict';

const uuid = require('node-uuid');
const Joi = require('joi');
const Controllers = require('../Controllers');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const Config = require('../Config');
const bookingType = Config.AppConstants.DATABASE.BOOKING_TYPE;

const createBooking = {
    method: 'POST',
    path: '/api/booking/createBooking',
    handler: function (request, reply) {
        let userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.BookingCtrl.createBooking(request.payload, userData, (err, data) => {
            if (err) {
                console.log(err);
                reply(UniversalFunctions.sendError(err));
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
            }
        });
    },
    config: {
        description: 'Create booking by Customer',
        tags: ['api', 'booking'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                bookingType: Joi.string().required().valid([bookingType.MULTIPLE, bookingType.SINGLE_DAY]),
                startDate: Joi.string().required().description('YYYY-MM-DD HH:mm:ss'),
                carDetails: Joi.string().optional().allow(''),
                address: Joi.string().required(),
                promoCode: Joi.string().optional().allow(''),
                latitude: Joi.number().required().min(-90).max(90),
                longitude: Joi.number().required().min(-180).max(180)
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};


module.exports = [
    createBooking
];