/**
 * Created by cl-macmini-88 on 8/24/16.
 */

'use strict';

const uuid = require('node-uuid');
const Joi = require('joi');
const Controllers = require('../Controllers');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const Config = require('../Config');


const addGeoLocation = {
    method: 'POST',
    path: '/api/admin/addGeoLocation',
    config: {
        description: "Add Location || GeoFencing",
        tags: ['api', 'admin'],
        //auth: 'AdminAuth',
        handler: function (request, reply) {
            Controllers.AdminCtrl.addGeoLocation(request.payload, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.CREATED, success)).code(200)
                }
            });
        },
        validate: {
            //headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                locationName: Joi.string().required(),
                coordinates: Joi.array().items(Joi.array().items(Joi.number().required()).required()).min(4).required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const checkInGeoArea = {
    method: 'POST',
    path: '/api/admin/checkInGeoArea',
    config: {
        description: "Add Location || GeoFencing",
        tags: ['api', 'admin'],
        //auth: 'AdminAuth',
        handler: function (request, reply) {
            Controllers.AdminCtrl.checkInGeoArea(request.payload, function (error, success) {
                if (error) {
                    console.log(error)
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, success)).code(200)
                }
            });
        },
        validate: {
            //headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                //locationName: Joi.string().required(),
                latitude: Joi.number().required().min(-90).max(90),
                longitude: Joi.number().required().min(-180).max(180)
                //coordinates: Joi.array().items(Joi.array().items(Joi.number().required()).required()).min(4).required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

module.exports = [
    addGeoLocation,
    checkInGeoArea
];