/**
 * Created by clicklabs113 on 3/10/16.
 */

var CustomerRoute = require('./CustomerRoute');
var SpRoute = require('./SpRoute');
var BookingRoute = require('./BookingRoute');
var AdminRoute = require('./AdminRoute');

var all = [].concat(CustomerRoute, SpRoute, BookingRoute, AdminRoute);
module.exports = all;