/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const async = require('async');
const mongoose = require('mongoose');
const Models = require('../Models');
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const createToken = require('../Utils/Token');
const Services = require('../Services');
const ERROR = Config.AppConstants.STATUS_MSG.ERROR;
const CodeGenerator = require('../Lib/CodeGenerator');
const UploadManager = require('../Lib/UploadManager');
const booking = Config.AppConstants.DATABASE.BOOKING_STATUS;
const moment = require('moment');

let spRegister = (payload, callb) => {
    let spDoesNotExist = false;
    let returnedData = {};
    let accessToken = null;
    let uniqueCode = null;
    let dataToSave = payload;
    let spData = null;
    let dataToUpdate = {};
    if (dataToSave.password)
        dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    payload.email = payload.email.toLowerCase();

    async.series([
        (cb) => {
            //verify email address format
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        (cb) => {
            //Validate phone No
            if (dataToSave.mobileNumber && dataToSave.mobileNumber.split('')[0] == 0) {
                cb(Config.AppConstants.STATUS_MSG.ERROR.INVALID_PHONE_NO_FORMAT);
            } else {
                cb();
            }
        },
        (cb) => {
            var criteria = {
                $or: [{
                    email: payload.email
                }, {
                    mobileNumber: payload.mobileNumber
                }]
            };
            Services.SpService.getSp(criteria, {}, {
                lean: true
            }, function (err, resp) {
                if (err) {
                    cb(err)
                } else {
                    if (resp && resp.length != 0) {
                        cb(ERROR.USER_ALREADY_REGISTERED)
                    } else {
                        spDoesNotExist = true;
                        cb()
                    }
                }
            });
            console.log("1");
        },
        (cb) => {
            CodeGenerator.generateUniqueCode(4, Config.AppConstants.DATABASE.USER_ROLES.SERVICE_PROVIDER, function (err, numberObj) {
                if (err) {
                    cb(err);
                } else {
                    if (!numberObj || numberObj.number == null) {
                        cb(ERROR.UNIQUE_CODE_LIMIT_REACHED);
                    } else {
                        uniqueCode = numberObj.number;
                        cb();
                    }
                }
            });
            console.log("2");
        },
        (cb) => {
            //Insert Into DB
            dataToSave.OTPCode = uniqueCode;
            dataToSave.mobileNumber = payload.mobileNumber;
            dataToSave.deviceToken = payload.deviceToken;
            dataToSave.deviceType = payload.deviceType;
            dataToSave.countryCode = "+91";
            //dataToSave.registrationDateTime = moment().add(330, 'm').format("YYYY-MM-DD, h:mm:ss a");
            dataToSave.registrationDateTime = moment().utcOffset(330);
            Services.SpService.createSp(dataToSave, (err, spDataFromDB) => {
                //Models.Customers(dataToSave).save(function (err, spDataFromDB) {
                console.log('hello', err, spDataFromDB);
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('customers.$mobileNo_1') > -1) {
                        cb(ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('customers.$email_1') > -1) {
                        cb(ERROR.EMAIL_ALREADY_EXIST);

                    } else {
                        cb(err)
                    }
                } else {
                    spData = spDataFromDB;
                    cb();
                }
            });
            console.log("3");
        },
        (cb) => {
            //Set Access
            if (spData) {
                var tokenData = {
                    id: spData._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.SERVICE_PROVIDER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR);
                console.log("4");
            }
        }
    ], (err, data) => {
        if (err) {
            callb(err)
        } else {
            dataToSave.accessToken = accessToken;
            return callb(null, dataToSave);
        }
    })
};

let spUpdateLocation = (payload, tokenData, callback) => {
    let dataToUpdate = {};
    dataToUpdate.spLocation = {
        "coordinates": [payload.longitude, payload.latitude],
        "type": "Point"
    };
    async.series([
            function (cb) {
                let query = {
                    _id: tokenData.id
                };
                let setQuery = {
                    $set: dataToUpdate
                };
                Services.SpService.updateSp(query, setQuery, (err, data) => {
                    if (!err) {
                        cb(null);
                    }
                    else {
                        cb(err);
                    }
                })
            }
        ],
        (error, result) => {
            if (error) {
                return callback(error);
            } else {
                return callback(null);
            }
        });
};


let addAndUpdateSpAvailability = (tokenData, payload, callback) => {
    let dataToSet = {};
    dataToSet = payload;
    let booksData;
    console.log("Check", payload.data);
    async.series([
            function (cb) {
                dataToSet.spId = tokenData._id;
                dataToSet.slots = payload.data;
                console.log(dataToSet);
                Services.SpAvailabilityService.createSpAvailability(dataToSet, function (err, data) {
                    if (!err) {
                        booksData = data;
                        cb();
                    } else {
                        cb(err);
                    }
                });
            }
        ],
        (error, result) => {
            if (error) {
                console.log(error);
                return callback(error);
            } else {
                return callback(null, {
                    booksData: booksData
                });
            }
        });
};

let updateBookingStatus = (payload, tokenData, callback) => {
    let dataToUpdate = {};
    let updatedBooking = {};
    let customerId;
    let customerData = {};
    let refCodeUsed = null;

    async.series([
            (cb) => {

                let query = {
                    _id: payload.bookingId
                };
                let setQuery = {
                    $set: {bookingStatus: payload.bookingStatus}
                };

                Services.BookingService.updateBookingModel(query, setQuery, {lean: true, new: true}, (err, data) => {
                    if (!err) {
                        updatedBooking = data;
                        customerId = updatedBooking.customerId;

                        console.log("refcode", refCodeUsed);
                        console.log("Updated Booking Data:", updatedBooking);

                        return cb(null);
                    }
                    else {
                        return cb(err);
                    }
                })
            },
            (cb)=> {
                if (payload.bookingStatus == "STARTED" && updatedBooking.bookingType == "SINGLE_DAY") {
                    let query = {
                        _id: tokenData._id
                    };
                    let setQuery = {
                        $set: {isBusy: true}
                    };

                    Services.SpService.updateSp(query, setQuery, {lean: true, new: true}, (err, data) => {
                        if (!err) {
                            console.log("Service Provider updated");

                            return cb(null);
                        }
                        else {
                            return cb(err);
                        }
                    })
                }
                else {
                    return cb(null);
                }
            },
            (cb)=> {
                if (payload.bookingStatus == "COMPLETED") {
                    let query = {
                        $and: [{
                            _id: updatedBooking.customerId
                        }, {
                            isRefCodeReg: true
                        }]
                    };

                    let setQuery = {
                        $inc: {totalCredits: 10}
                    };

                    Services.CustomerService.updateCustomer(query, setQuery, {lean: true, new: true}, (err, data) => {
                        if (!err) {

                            console.log("****************");
                            return cb(null);
                        }
                        else {
                            cb(err);
                        }
                    });
                } else {
                    return cb(null);
                }
            },
            (cb) => {
                let query = {
                    $and: [{
                        _id: updatedBooking.customerId
                    }, {
                        isRefCodeReg: true
                    }]
                };

                var options = {
                    lean: true
                };
                Services.CustomerService.getCustomer(query, {}, options, (err, data)=> {
                    if (err) {
                        cb(err);
                    } else {
                        customerData = data && data[0] || null;
                        if (customerData != null) {
                            refCodeUsed = customerData.refCodeUsed;
                        }

                        console.log("Updated Customer Data:", customerData);
                        console.log("refCodeUsed", refCodeUsed);
                        cb(null);
                    }
                });
            },
            (cb)=> {
                if (payload.bookingStatus == "COMPLETED") {
                    let query = {
                        $and: [{
                            _id: updatedBooking.customerId
                        }, {
                            isRefCodeReg: true
                        }]
                    };

                    let setQuery = {
                        $set: {isRefCodeReg: false}
                    };
                    Services.CustomerService.updateCustomer(query, setQuery, {lean: true, new: true}, (err, data) => {
                        if (!err) {
                            console.log("****************");
                            return cb(null);
                        }
                        else {
                            cb(err);
                        }
                    });
                } else {
                    return cb(null);
                }
            },
            (cb)=> {
                if (refCodeUsed != null) {
                    let query = {
                        referralCode: refCodeUsed
                    };

                    let setQuery = {
                        $inc: {totalCredits: 10}
                    };

                    Services.CustomerService.updateCustomer(query, setQuery, {lean: true, new: true}, (err, data) => {
                        if (!err) {
                            console.log("Executed: **********");
                            return cb(null);
                        }
                        else {
                            cb(err);
                        }
                    });
                } else {
                    return cb(null);
                }
            },
            (cb)=> {
                if (payload.bookingStatus == "COMPLETED" && updatedBooking.bookingType == "SINGLE_DAY") {
                    let query = {
                        _id: tokenData._id
                    };
                    let setQuery = {
                        $set: {isBusy: false}
                    };

                    Services.SpService.updateSp(query, setQuery, {lean: true, new: true}, (err, data) => {
                        if (!err) {
                            console.log("Service Provider updated");

                            return cb(null);
                        }
                        else {
                            return cb(err);
                        }
                    })
                }
                else {
                    return cb(null);
                }
            }
        ],
        (error, result) => {
            if (error) {
                return callback(error);
            } else {
                return callback(null, updatedBooking);
            }
        });
};

const spLogin = (payload, callBackRoute) => {
    let userFound = false;
    let successLogin = false;
    let accessToken = null;
    let returnedData = {};

    async.series([
        //function (cb) {
        //    //verify email address
        //    if (!UniversalFunctions.verifyEmailFormat(payload.email)) {
        //        cb(ERROR.INVALID_EMAIL);
        //    } else {
        //        cb();
        //    }
        //},
        (cb) => {
            let criteria = {
                $or: [{
                    email: payload.emailOrPhone
                }, {
                    mobileNumber: payload.emailOrPhone
                }]
            };
            let option = {
                lean: true
            };
            Services.SpService.getSp(criteria, {}, option, (err, result) => {
                if (err) {
                    cb(err)
                } else {
                    userFound = result && result[0] || null;
                    returnedData = result;
                    cb();
                }
            });
        },
        (cb) => {

            if (!userFound) {

                cb(ERROR.UNREGISTERED_EMAIL)
            } else {
                if (userFound && userFound.password != UniversalFunctions.CryptData(payload.password)) {
                    cb(ERROR.INCORRECT_PASSWORD)
                } else {
                    successLogin = true;
                    cb();
                }
            }
        },
        (cb) => {
            if (successLogin) {
                let tokenData = {
                    id: userFound._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.SERVICE_PROVIDER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            accessToken = output && output.accessToken;
                            cb();
                        } else {

                            cb(ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR);

            }

        }
    ], (err, data) => {
        if (err) {
            callBackRoute(err)
        } else {
            var userDetails = UniversalFunctions.deleteUnnecessaryUserData(userFound);
            userDetails.accessToken = accessToken;
            callBackRoute(null, userDetails);
        }
    })
};

const spLogout = (userData, callbackRoute) => {
    async.series([
            (callback) => {
                var condition = {
                    _id: userData.id
                };
                var dataToUpdate = {
                    $unset: {
                        accessToken: 1,
                        deviceToken: 1
                    }
                };
                Services.SpService.updateSp(condition, dataToUpdate, {
                    new: true
                }, (err, result) => {
                    if (err) {
                        callback(err);
                    } else {
                        callback();
                    }
                });
            }
        ],
        (error, result) => {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute();
            }
        });
};

var spGetProfile = function (data, callbackRoute) {
    var query = {
        _id: data.id
    };
    var projection = {
        fullName: 1,
        email: 1,
        countryCode: 1,
        mobileNumber: 1,
        isPhoneVerified: 1,
        registrationDateUTC: 1,
        highestQualification: 1
    };
    var options = {
        lean: true
    };
    Services.SpService.getSp(query, projection, options, (err, data)=> {
        if (err) {
            callbackRoute(err);
        } else {
            var spdata = data && data[0] || null;
            console.log("spData-------->>>" + JSON.stringify(spdata));
            if (spdata == null) {
                callbackRoute(ERROR.INVALID_TOKEN);
            } else {
                callbackRoute(null, spdata);
            }
        }
    });
};

const spVerifyOTP = (payload, tokenData, callback) => {
    if (!payload || !tokenData._id) {
        callback(ERROR.IMP_ERROR)
    } else {
        //    var newNumberToVerify = queryData.countryCode + '-' + queryData.mobileNo;
        async.series([
            (cb) => {
                //Check verification code :
                if (payload.OTPCode == tokenData.OTPCode) {
                    cb();
                } else {
                    cb(ERROR.INVALID_CODE)
                }
            },
            (cb) => {
                //trying to update customer
                var criteria = {
                    _id: tokenData.id,
                    OTPCode: payload.OTPCode
                };
                var setQuery = {
                    $set: {
                        isPhoneVerified: true
                    },
                    $unset: {
                        OTPCode: 1
                    }
                };
                var options = {
                    new: true
                };
                //console.log('updating>>>', criteria, setQuery, options);
                Services.SpService.updateSp(criteria, setQuery, options, function (err, updatedData) {
                    console.log('verify otp callback result', err, updatedData);
                    if (err) {
                        cb(err)
                    } else {
                        if (!updatedData) {
                            cb(ERROR.INVALID_CODE)
                        } else {
                            cb();
                        }
                    }
                });
            }
        ], (err, result) => {
            if (err) {
                callback(err)
            } else {
                callback();
            }

        });
    }
};

const spResendOTP = (userData, callback) => {

    let mobileNumber = userData.mobileNumber;
    let countryCode = userData.countryCode;
    let dataFound;
    if (!mobileNumber) {
        callback(ERROR.IMP_ERROR);
    } else {
        var uniqueCode = null;
        async.series([
            (cb) => {
                var query = {
                    mobileNumber: userData.mobileNumber
                };
                var projection = {
                    _id: 1,
                    mobileNumber: 1,
                    isPhoneVerified: 1,
                    countryCode: 1
                };
                Services.SpService.getSp(query, projection, {}, function (err, data) {
                    if (err) {
                        cb(ERROR.PASSWORD_CHANGE_REQUEST_INVALID);
                    } else {
                        dataFound = data && data[0] || null;
                        if (dataFound == null) {
                            cb(ERROR.PHONENUMBER_NOT_REGISTERED);
                        } else {
                            if (dataFound.isPhoneVerified == true) {
                                cb(ERROR.PHONE_VERIFICATION_COMPLETE);
                            } else {
                                cb();
                            }

                        }
                    }
                });
            },
            (cb) => {
                CodeGenerator.generateUniqueCode(4, Config.AppConstants.DATABASE.USER_ROLES.SERVICE_PROVIDER, function (err, numberObj) {
                    if (err) {
                        cb(err);
                    } else {
                        if (!numberObj || numberObj.number == null) {
                            cb(ERROR.UNIQUE_CODE_LIMIT_REACHED);
                        } else {
                            uniqueCode = numberObj.number;
                            cb();
                        }
                    }
                })
            },
            (cb) => {
                var criteria = {
                    _id: userData.id
                };
                var setQuery = {
                    $set: {
                        OTPCode: uniqueCode
                    }
                };
                var options = {
                    lean: true
                };
                Services.SpService.updateSp(criteria, setQuery, options, cb);
                //console.log("New OTTTTTTTTTTTTP Code", uniqueCode);
            }
            //, function (cb) {
            //    var criteria = {
            //        _id: userData.id
            //    };
            //    var projection = {
            //        OTPCode: 1
            //    };
            //    Services.CustomerService.getCustomer(criteria, projection, {lean: true}, cb);
            //}
        ], (err, result) => {
            callback(err, null);
        })
    }
};

const spChangePassword = (payload, tokenData, callbackRoute) => {
    let oldPassword = UniversalFunctions.CryptData(payload.oldPassword);
    let newPassword = UniversalFunctions.CryptData(payload.newPassword);
    async.series([
            (callback) => {
                let query = {
                    _id: tokenData.id
                };
                let projection = {
                    password: 1
                };
                let options = {
                    lean: true
                };
                Services.SpService.getSp(query, projection, options, (err, data) => {
                    if (err) {
                        callback(err);
                    } else {
                        let customerData = data && data[0] || null;

                        if (customerData == null) {
                            callback(ERROR.NOT_FOUND);
                        } else {
                            if (data[0].password == oldPassword && data[0].password != newPassword) {
                                callback(null);
                            } else if (data[0].password != oldPassword) {
                                callback(ERROR.WRONG_PASSWORD)
                            } else if (data[0].password == newPassword) {
                                callback(ERROR.NOT_UPDATE)
                            }
                        }
                    }
                });
            },
            (callback) => {
                let dataToUpdate = {
                    $set: {
                        'password': UniversalFunctions.CryptData(payload.newPassword)
                    }
                };
                let condition = {
                    _id: tokenData.id
                };
                Services.SpService.updateSp(condition, dataToUpdate, {}, (err, user) => {

                    if (err) {
                        callback(err);
                    } else {
                        if (!user || user.length == 0) {
                            callback(ERROR.NOT_FOUND);
                        } else {
                            callback(null);
                        }
                    }
                });
            }
        ],
        (error, result)=> {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};

const spForgetPassword = (payload, callBackRoute) => {
    var dataFound = null;
    var code;
    var forgotDataEntry;
    var tempPassword = null;
    async.series([
        (callback) => {
            var query = {
                mobileNumber: payload.mobileNumber
            };
            var projection = {
                _id: 1,
                mobileNumber: 1,
                isPhoneVerified: 1,
                countryCode: 1
            };
            Services.SpService.getSp(query, projection, {
                lean: true
            }, (err, resp) => {
                if (err) {
                    cb(err);
                } else {
                    dataFound = resp && resp[0] || null;
                    if (dataFound == null) {
                        callback(ERROR.PHONENUMBER_NOT_REGISTERED);
                    } else {
                        if (dataFound.isPhoneVerified == false) {
                            callback(ERROR.PHONE_VERIFICATION);
                        } else {
                            callback();
                        }
                    }
                }
            });
            /*                Services.BooksUserService.getBooksUser(query, {
             _id: 1,
             mobileNumber: 1,
             isPhoneVerified: 1,
             countryCode: 1
             }, {}, function (err, data) {
             if (err) {
             console.log(err);
             callback(ERROR.PASSWORD_CHANGE_REQUEST_INVALID);
             } else {
             dataFound = data && data[0] || null;
             if (dataFound == null) {
             callback(ERROR.PHONENUMBER_NOT_REGISTERED);
             } else {
             if (dataFound.isPhoneVerified == false) {
             callback(ERROR.PHONE_VERIFICATION);
             } else {
             callback();
             }

             }
             }
             });*/
        },
        (callback) => {
            console.log("8");
            CodeGenerator.generateRandPass(8, function (err, numberObj) {
                if (err) {
                    callback(err);
                } else {
                    console.log("numberrrrrrrr", numberObj);
                    if (!numberObj || numberObj.number == null) {
                        callback(ERROR.UNIQUE_CODE_LIMIT_REACHED);
                    } else {
                        code = numberObj.number;
                        tempPassword = code;
                        code = UniversalFunctions.CryptData(code);
                        //console.log("codeeeeeeeeee:", numberObj.number);
                        callback();
                    }
                }
            })
        },
        function (callback) {
            var dataToUpdate = {
                password: code
            };
            var query = {
                _id: dataFound._id
            };
            Services.SpService.updateSp(query, dataToUpdate, {}, function (err, data) {
                if (err) {
                    callback(err);
                } else {
                    callback();
                }
            });
        }
    ], (error, result)=> {
        if (error) {
            return callBackRoute(error);
        } else {
            return callBackRoute(null, tempPassword);
        }
    })
};

module.exports = {
    spRegister: spRegister,
    spUpdateLocation: spUpdateLocation,
    updateBookingStatus: updateBookingStatus,
    spLogin: spLogin,
    spLogout: spLogout,
    spGetProfile: spGetProfile,
    spVerifyOTP: spVerifyOTP,
    spResendOTP: spResendOTP,
    spChangePassword: spChangePassword,
    spForgetPassword: spForgetPassword
};