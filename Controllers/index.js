/**
 * Created by SONY on 3/12/2016.
 */
module.exports = {
    CustomerCtrl: require('./CustomerCtrl'),
    SpCtrl: require('./SpCtrl'),
    BookingCtrl: require('./BookingCtrl'),
    AdminCtrl: require('./AdminCtrl')
};