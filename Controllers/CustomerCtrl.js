/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const async = require('async');
const mongoose = require('mongoose');
const Models = require('../Models');
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const createToken = require('../Utils/Token');
const Services = require('../Services');
const ERROR = Config.AppConstants.STATUS_MSG.ERROR;
const CodeGenerator = require('../Lib/CodeGenerator');
const UploadManager = require('../Lib/UploadManager');
const moment = require('moment');

let customerRegister = (payload, callb) => {
    let customerDoesNotExist = false;
    let returnedData = {};
    let accessToken = null;
    let uniqueCode = null;
    let dataToSave = payload;
    let customerData = null;
    let dataToUpdate = {};
    if (dataToSave.password)
        dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    payload.email = payload.email.toLowerCase();

    if (dataToSave.referralCode) {
        dataToSave.isRefCodeReg = true;
        dataToSave.refCodeUsed = payload.referralCode
    }

    async.series([
        (cb) => {
            //verify email address format
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        (cb) => {
            //Validate phone No
            if (dataToSave.mobileNumber && dataToSave.mobileNumber.split('')[0] == 0) {
                cb(Config.AppConstants.STATUS_MSG.ERROR.INVALID_PHONE_NO_FORMAT);
            } else {
                cb();
            }
        },
        (cb) => {
            var criteria = {
                $or: [{
                    email: payload.email
                }, {
                    mobileNumber: payload.mobileNumber
                }]
            };
            Services.CustomerService.getCustomer(criteria, {}, {
                lean: true
            }, function (err, resp) {
                if (err) {
                    cb(err)
                } else {
                    if (resp && resp.length != 0) {
                        cb(ERROR.USER_ALREADY_REGISTERED)
                    } else {
                        customerDoesNotExist = true;
                        cb()
                    }
                }
            })
        },
        (cb)=> {
            if (payload.referralCode != '') {
                console.log("Criteria", payload.referralCode);
                Services.CustomerService.getCustomer(payload.referralCode, {}, {
                    lean: true
                }, function (err, resp) {
                    if (err) {
                        cb(err)
                    } else {
                        if (resp && resp.length != 0) {
                            cb()
                        } else {
                            cb(ERROR.INVALID_REFERRAL_CODE)
                        }
                    }
                })
            }
            else {
                cb();
            }

        },
        (cb) => {
            CodeGenerator.generateUniqueCode(4, Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                if (err) {
                    cb(err);
                } else {
                    if (!numberObj || numberObj.number == null) {
                        cb(ERROR.UNIQUE_CODE_LIMIT_REACHED);
                    } else {
                        uniqueCode = numberObj.number;
                        cb();
                    }
                }
            })
        },
        (cb) => {
            //Insert Into DB
            dataToSave.OTPCode = uniqueCode;
            dataToSave.mobileNumber = payload.mobileNumber;
            dataToSave.deviceToken = payload.deviceToken;
            dataToSave.deviceType = payload.deviceType;
            dataToSave.countryCode = "+91";
            //dataToSave.registrationDateTime = moment().add(330, 'm').format("YYYY-MM-DD, h:mm:ss a");
            dataToSave.registrationDateTime = moment().utcOffset("+05:30");
            //dataToSave.registrationDateTime = moment();

            let refCode = dataToSave.fullName.substring(0, 3) + UniversalFunctions.generateReferralCode();
            dataToSave.referralCode = refCode.toString().toUpperCase();

            Services.CustomerService.createCustomer(dataToSave, function (err, customerDataFromDB) {

                console.log('hello', err, customerDataFromDB);
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('customers.$mobileNo_1') > -1) {
                        cb(ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('customers.$email_1') > -1) {
                        cb(ERROR.EMAIL_ALREADY_EXIST);

                    } else {
                        cb(err)
                    }
                } else {
                    customerData = customerDataFromDB;
                    cb();
                }
            });
        },
        (cb) => {
            //Set Access
            if (customerData) {
                var tokenData = {
                    id: customerData._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR)
            }
        }
    ], (err, data) => {
        if (err) {
            callb(err)
        } else {

            dataToSave.accessToken = accessToken;
            return callb(null, dataToSave);
        }
    })
};

const customerLogin = (payload, callBackRoute) => {
    let userFound = false;
    let successLogin = false;
    let accessToken = null;
    let returnedData = {};
    let updatedUserDetails = null;
    let defaultAddress = null;

    async.series([
        //function (cb) {
        //    //verify email address
        //    if (!UniversalFunctions.verifyEmailFormat(payload.email)) {
        //        cb(ERROR.INVALID_EMAIL);
        //    } else {
        //        cb();
        //    }
        //},
        (cb) => {
            let criteria = {
                $or: [{
                    email: payload.emailOrPhone
                }, {
                    mobileNumber: payload.emailOrPhone
                }]
            };
            let option = {
                lean: true
            };
            Services.CustomerService.getCustomer(criteria, {}, option, (err, result) => {
                if (err) {
                    cb(err)
                } else {
                    userFound = result && result[0] || null;
                    returnedData = result;
                    cb();
                }
            });
        },
        (cb) => {
            //validations
            if (!userFound) {
                //cb("You are not registered with us.");
                cb(ERROR.UNREGISTERED_EMAIL)
            } else {
                /*                if (userFound && userFound.isPhoneVerified == false) {
                 cb(ERROR.NOT_VERIFIED)
                 }*/

                if (userFound && userFound.password != UniversalFunctions.CryptData(payload.password)) {
                    //cb("Incorrect Password");
                    cb(ERROR.INCORRECT_PASSWORD)
                } else {
                    successLogin = true;
                    cb();
                }

            }
        },
        (cb) => {
            if (successLogin) {
                let tokenData = {
                    id: userFound._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            accessToken = output && output.accessToken;
                            cb();
                        } else {
                            //cb("Error 2")
                            cb(ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR);
                //cb("Error 3")
            }

        }
    ], function (err, data) {
        if (err) {
            callBackRoute(err)
        } else {
            var userDetails = UniversalFunctions.deleteUnnecessaryUserData(userFound);
            userDetails.accessToken = accessToken;
            callBackRoute(null, userDetails);
        }
    })
};

const customerLogout = (userData, callbackRoute) => {
    async.series([
            (callback) => {
                var condition = {
                    _id: userData.id
                };
                var dataToUpdate = {
                    $unset: {
                        accessToken: 1,
                        deviceToken: 1
                    }
                };
                Services.CustomerService.updateCustomer(condition, dataToUpdate, {
                    new: true
                }, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback();
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute();
            }
        });
};

const getProfile = function (data, callbackRoute) {
    var query = {
        _id: data.id
    };
    var projection = {
        fullName: 1,
        email: 1,
        countryCode: 1,
        mobileNumber: 1,
        isPhoneVerified: 1,
        registrationDateUTC: 1,
        highestQualification: 1
    };
    var options = {
        lean: true
    };
    Services.CustomerService.getCustomer(query, projection, options, function (err, data) {
        if (err) {
            callbackRoute(err);
        } else {
            var customerData = data && data[0] || null;
            console.log("customerData-------->>>" + JSON.stringify(customerData));
            if (customerData == null) {
                callbackRoute(ERROR.INVALID_TOKEN);
                //callbackRoute("Invalid Token");
            } else {
                callbackRoute(null, customerData);
            }
        }
    });
};

const verifyOTP = (payload, tokenData, callback) => {
    if (!payload || !tokenData._id) {
        callback(ERROR.IMP_ERROR)
    } else {
        //    var newNumberToVerify = queryData.countryCode + '-' + queryData.mobileNo;
        async.series([
            (cb) => {
                //Check verification code :
                if (payload.OTPCode == tokenData.OTPCode) {
                    cb();
                } else {
                    cb(ERROR.INVALID_CODE)
                }
            },
            (cb) => {
                //trying to update customer
                var criteria = {
                    _id: tokenData.id,
                    OTPCode: payload.OTPCode
                };
                var setQuery = {
                    $set: {
                        isPhoneVerified: true
                    },
                    $unset: {
                        OTPCode: 1
                    }
                };
                var options = {
                    new: true
                };
                console.log('updating>>>', criteria, setQuery, options);
                Services.CustomerService.updateCustomer(criteria, setQuery, options, function (err, updatedData) {
                    console.log('verify otp callback result', err, updatedData);
                    if (err) {
                        cb(err)
                    } else {
                        if (!updatedData) {
                            cb(ERROR.INVALID_CODE)
                        } else {
                            cb();
                        }
                    }
                });
            }
        ], function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback();
            }

        });
    }
};

const resendOTP = (userData, callback) => {

    let mobileNumber = userData.mobileNumber;
    let countryCode = userData.countryCode;
    let dataFound;
    if (!mobileNumber) {
        callback(ERROR.IMP_ERROR);
    } else {
        var uniqueCode = null;
        async.series([
            function (cb) {
                var query = {
                    mobileNumber: userData.mobileNumber
                };
                var projection = {
                    _id: 1,
                    mobileNumber: 1,
                    isPhoneVerified: 1,
                    countryCode: 1
                };
                Services.CustomerService.getCustomer(query, projection, {}, function (err, data) {
                    if (err) {
                        cb(ERROR.PASSWORD_CHANGE_REQUEST_INVALID);
                    } else {
                        dataFound = data && data[0] || null;
                        if (dataFound == null) {
                            cb(ERROR.PHONENUMBER_NOT_REGISTERED);
                        } else {
                            if (dataFound.isPhoneVerified == true) {
                                cb(ERROR.PHONE_VERIFICATION_COMPLETE);
                            } else {
                                cb();
                            }

                        }
                    }
                });
            },
            function (cb) {
                CodeGenerator.generateUniqueCode(4, Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                    if (err) {
                        cb(err);
                    } else {
                        if (!numberObj || numberObj.number == null) {
                            cb(ERROR.UNIQUE_CODE_LIMIT_REACHED);
                        } else {
                            uniqueCode = numberObj.number;
                            cb();
                        }
                    }
                })
            },
            function (cb) {
                var criteria = {
                    _id: userData.id
                };
                var setQuery = {
                    $set: {
                        OTPCode: uniqueCode
                    }
                };
                var options = {
                    lean: true
                };
                Services.CustomerService.updateCustomer(criteria, setQuery, options, cb);
                //console.log("New OTTTTTTTTTTTTP Code", uniqueCode);
            }
            //, function (cb) {
            //    var criteria = {
            //        _id: userData.id
            //    };
            //    var projection = {
            //        OTPCode: 1
            //    };
            //    Services.CustomerService.getCustomer(criteria, projection, {lean: true}, cb);
            //}
        ], function (err, result) {
            callback(err, null);
        })
    }
};

const changePassword = function (payload, tokenData, callbackRoute) {
    let oldPassword = UniversalFunctions.CryptData(payload.oldPassword);
    let newPassword = UniversalFunctions.CryptData(payload.newPassword);
    async.series([
            function (callback) {
                let query = {
                    _id: tokenData.id
                };
                let projection = {
                    password: 1
                };
                let options = {
                    lean: true
                };
                Services.CustomerService.getCustomer(query, projection, options, function (err, data) {
                    if (err) {
                        callback(err);
                    } else {
                        var customerData = data && data[0] || null;
                        console.log("customerData-------->>>" + JSON.stringify(customerData));
                        if (customerData == null) {
                            callback(ERROR.NOT_FOUND);
                        } else {
                            if (data[0].password == oldPassword && data[0].password != newPassword) {
                                callback(null);
                            } else if (data[0].password != oldPassword) {
                                callback(ERROR.WRONG_PASSWORD)
                            } else if (data[0].password == newPassword) {
                                callback(ERROR.NOT_UPDATE)
                            }
                        }
                    }
                });
            },
            function (callback) {
                var dataToUpdate = {
                    $set: {
                        'password': UniversalFunctions.CryptData(payload.newPassword)
                    }
                };
                var condition = {
                    _id: tokenData.id
                };
                Services.CustomerService.updateCustomer(condition, dataToUpdate, {}, function (err, user) {
                    console.log("customerData-------->>>" + JSON.stringify(user));
                    if (err) {
                        callback(err);
                    } else {
                        if (!user || user.length == 0) {
                            callback(ERROR.NOT_FOUND);
                        } else {
                            callback(null);
                        }
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};

var forgetPassword = function (payload, callBackRoute) {
    var dataFound = null;
    var code;
    var forgotDataEntry;
    var tempPassword = null;
    async.series([
        function (callback) {
            var query = {
                $or: [{
                    email: payload.emailOrPhone
                }, {
                    mobileNumber: payload.emailOrPhone
                }]
            };

            Services.CustomerService.getCustomer(query, {}, {
                lean: true
            }, function (err, resp) {
                if (err) {
                    cb(err);
                } else {
                    dataFound = resp && resp[0] || null;
                    if (dataFound == null) {
                        callback(ERROR.UNREGISTERED_EMAIL);
                    } else {
                        if (dataFound.isPhoneVerified == false) {
                            //callback(ERROR.PHONE_VERIFICATION);
                            callback();
                        } else {
                            callback();
                        }
                    }
                }
            });
            /*                Services.BooksUserService.getBooksUser(query, {
             _id: 1,
             mobileNumber: 1,
             isPhoneVerified: 1,
             countryCode: 1
             }, {}, function (err, data) {
             if (err) {
             console.log(err);
             callback(ERROR.PASSWORD_CHANGE_REQUEST_INVALID);
             } else {
             dataFound = data && data[0] || null;
             if (dataFound == null) {
             callback(ERROR.PHONENUMBER_NOT_REGISTERED);
             } else {
             if (dataFound.isPhoneVerified == false) {
             callback(ERROR.PHONE_VERIFICATION);
             } else {
             callback();
             }

             }
             }
             });*/
        },
        function (callback) {
            console.log("8");
            CodeGenerator.generateRandPass(8, function (err, numberObj) {
                if (err) {
                    callback(err);
                } else {
                    console.log("numberrrrrrrr", numberObj);
                    if (!numberObj || numberObj.number == null) {
                        callback(ERROR.UNIQUE_CODE_LIMIT_REACHED);
                    } else {
                        code = numberObj.number;
                        tempPassword = code;
                        code = UniversalFunctions.CryptData(code);
                        //console.log("codeeeeeeeeee:", numberObj.number);
                        callback();
                    }
                }
            })
        },
        function (callback) {
            var dataToUpdate = {
                password: code
            };
            var query = {
                _id: dataFound._id
            };
            Services.CustomerService.updateCustomer(query, dataToUpdate, {}, (err, data)=> {
                if (err) {
                    callback(err);
                } else {
                    callback();
                }
            });
        }
    ], function (error, result) {
        if (error) {
            return callBackRoute(error);
        } else {
            return callBackRoute(null, tempPassword);
        }
    })
};

const getAllSp = (callbackRoute) => {

    var query = {};
    var projection = {};

    var options = {
        lean: true
    };
    Services.SpService.getSp(query, projection, options, function (err, data) {
        if (err) {
            callbackRoute(err);
        } else {
            var customerData = data && data[0] || null;
            console.log("customerData-------->>>" + JSON.stringify(customerData));
            if (customerData == null) {
                callbackRoute(ERROR.INVALID_TOKEN);
                //callbackRoute("Invalid Token");
            } else {
                callbackRoute(null, customerData);
            }
        }
    });
};

const getAllBooking = function (data, callbackRoute) {
    let options = {
        lean: true
    };
    let query = {
        customerId: data.id
    };
    Services.BookingService.getBookingModel(query, {}, options, function (err, data) {
        if (err) {
            callbackRoute(err);
        } else {
            var customerData = data && data || null;
            console.log("customerData-------->>>" + JSON.stringify(customerData));
            if (customerData == null) {
                callbackRoute(null, null);
                //callbackRoute("Invalid Token");
            } else {
                callbackRoute(null, customerData);
            }
        }
    });
};

const rebookSession = function (payload, data, callbackRoute) {
    let dataToUpdate = {};
    let serviceProviderIds = [];
    let spData = {};
    let bookingDetails = {};
    let bookingData;

    async.series([
            (cb) => {
                let options = {
                    lean: true
                };
                let query = {
                    _id: payload.bookingId
                };
                Services.BookingService.getBookingModel(query, {}, options, (err, data) => {
                    if (err) {
                        cb(err);
                    } else {
                        bookingData = data && data[0] || null;

                        if (bookingData == null) {
                            cb("No booking found");
                        } else {
                            cb(null);
                        }
                    }
                });
            },
            (cb) => {
                dataToUpdate.bookingLocation = {
                    "coordinates": [bookingData.longitude, bookingData.latitude],
                    "type": "Point"
                };
                dataToUpdate.customerId = bookingData.customerId;
                dataToUpdate.carDetails = bookingData.carDetails;

                dataToUpdate.startDate = moment(bookingData.endDate).add(1, 'days').format("YYYY-MM-DD HH:mm:ss");
                //dataToUpdate.startDate = moment(new Date(payload.startDate)).format('YYYY-MM-DD HH:mm:ss');
                if (payload.bookingType == "SINGLE_DAY") {
                    dataToUpdate.bookingType = "SINGLE_DAY";
                    dataToUpdate.endDate = moment(dataToUpdate.startDate).add(1, 'h').format('YYYY-MM-DD HH:mm:ss');
                    dataToUpdate.bookingPrice = 110;
                }
                else {
                    dataToUpdate.bookingType = "MULTIPLE";
                    dataToUpdate.endDate = moment(dataToUpdate.startDate).add(14, 'days').format('YYYY-MM-DD HH:mm:ss');
                    dataToUpdate.endDate = moment(dataToUpdate.startDate).add(1, 'h').format('YYYY-MM-DD HH:mm:ss');
                    dataToUpdate.bookingPrice = 410;
                }

                dataToUpdate.requestedAt = moment();
                dataToUpdate.address = bookingData.address;
                Services.BookingService.createBookingModel(dataToUpdate, (err, data) => {
                    if (!err) {
                        bookingDetails = data;
                        console.log("++++++++++++++++", bookingDetails);
                        cb();
                    }
                    else {
                        cb(err);
                    }
                })
            },
            (cb) => {
                //create a query in this way so that it finds SP are in defined kms
                let longVal = payload.longitude;
                let latVal = payload.latitude;

                let arr = [longVal, latVal];

                let query = {
                    spLocation: {
                        $near: {
                            $maxDistance: 5 * 1000,
                            $spherical: true,
                            $geometry: {
                                type: 'Point',
                                coordinates: arr
                            }
                        }
                    }
                };
                Services.SpService.getSp(query, {}, {}, function (err, data) {
                    if (err) {
                        cb(err);
                    }
                    else {
                        if (data && data.length > 0) {
                            for (let sp = 0; sp < data.length; sp++) {
                                serviceProviderIds.push(data[sp]._id);
                            }
                            console.log("**************", serviceProviderIds);
                            return cb();
                        }
                        else {
                            console.log('Service Provider not available.');
                            cb();
                        }
                    }
                });
            },
            (cb) => {
                var busySpList = [];
                Services.SpService.getSp({isBusy: true}, {}, {}, function (err, data) {
                    if (err) {
                        cb(err);
                    }
                    else {
                        if (data && data.length > 0) {
                            for (let sp = 0; sp < data.length; sp++) {
                                busySpList.push(data[sp]._id);
                            }
                            console.log("**************", busySpList);

                            //removing  busy Sp's from our booking list SP's

                            for (let i = 0; i < busySpList.length; i++) {
                                for (var j = 0; j < serviceProviderIds.length; j++) {
                                    if (JSON.stringify(busySpList[i]) == JSON.stringify(serviceProviderIds[j])) {
                                        serviceProviderIds.splice(j, 1);
                                        console.log("Updated list of SP", serviceProviderIds);
                                    }
                                }
                            }
                            return cb(null);
                        }
                        else {
                            console.log('All Service Providers are free');
                            return cb(null);
                        }
                    }
                });
            },
            (cb)=> {
                var query = {
                    _id: bookingDetails._id
                };

                var dataToUpdate = {
                    $set: {spList: serviceProviderIds}
                };

                var options = {};

                Services.BookingService.updateBookingModel(query, dataToUpdate, options, function (err, data) {
                    if (err)
                        cb(err);
                    else if (data) {
                        console.log("Booking Details", data);
                        bookingDetails = data;
                        cb(null);
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null, bookingDetails);
            }
        });
};


module.exports = {
    customerRegister: customerRegister,
    customerLogin: customerLogin,
    customerLogout: customerLogout,
    getProfile: getProfile,
    verifyOTP: verifyOTP,
    resendOTP: resendOTP,
    changePassword: changePassword,
    forgetPassword: forgetPassword,
    getAllSp: getAllSp,
    getAllBooking: getAllBooking,
    rebookSession: rebookSession
};