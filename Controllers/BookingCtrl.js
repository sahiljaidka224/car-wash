/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const async = require('async');
const mongoose = require('mongoose');
const Models = require('../Models');
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const Services = require('../Services');
const CodeGenerator = require('../Lib/CodeGenerator');
const UploadManager = require('../Lib/UploadManager');
const bookingType = Config.AppConstants.DATABASE.BOOKING_TYPE;

const moment = require('moment');

let spUpdateLocation = (payload, tokenData, callback) => {
    let dataToUpdate = {};
    dataToUpdate.spLocation = {
        "coordinates": [payload.longitude, payload.latitude],
        "type": "Point"
    };
    async.series([
            function (cb) {
                let query = {
                    _id: tokenData.id
                };
                let setQuery = {
                    $set: dataToUpdate
                };
                Services.SpService.updateSp(query, setQuery, (err, data) => {
                    if (!err) {
                        return cb(null);
                    }
                    else {
                        return cb(err);
                    }
                })
            }
        ],
        (error, result) => {
            if (error) {
                return callback(error);
            } else {
                return callback(null);
            }
        });
};

let createBooking = (payload, tokenData, callback) => {
    let dataToUpdate = {};
    let serviceProviderIds = [];
    let spData = {};
    let bookingDetails = {};
    dataToUpdate.bookingLocation = {
        "coordinates": [payload.longitude, payload.latitude],
        "type": "Point"
    };
    dataToUpdate.customerId = tokenData._id;
    dataToUpdate.carDetails = payload.carDetails;

    dataToUpdate.startDate = moment(payload.startDate).format("YYYY-MM-DD HH:mm:ss");
    //dataToUpdate.startDate = moment(new Date(payload.startDate)).format('YYYY-MM-DD HH:mm:ss');
    if (payload.bookingType == bookingType.SINGLE_DAY) {
        dataToUpdate.bookingType = bookingType.SINGLE_DAY;
        dataToUpdate.endDate = moment(payload.startDate).add(1,'h').format('YYYY-MM-DD HH:mm:ss');
        dataToUpdate.bookingPrice = 110;
    }
    else {
        dataToUpdate.bookingType = bookingType.MULTIPLE;
        //dataToUpdate.endDate = moment(payload.startDate).add(14, 'days').format("YYYY-MM-DD");
        dataToUpdate.endDate = moment(payload.startDate).add(14, 'days').format('YYYY-MM-DD HH:mm:ss');
        dataToUpdate.endDate = moment(payload.startDate).add(1, 'h').format('YYYY-MM-DD HH:mm:ss');
        dataToUpdate.bookingPrice = 410;
    }

    dataToUpdate.requestedAt = moment();
    //dataToUpdate.startTime = moment(payload.startTime, "hmm").format("HH:mm");
    //dataToUpdate.endTime = moment(payload.startTime, "hmm").add(1, 'h').format("HH:mm");
    dataToUpdate.address = payload.address;

    async.series([
            function (cb) {
                Services.BookingService.createBookingModel(dataToUpdate, (err, data) => {
                    if (!err) {
                        bookingDetails = data;
                        console.log("++++++++++++++++", bookingDetails);
                        cb();
                    }
                    else {
                        cb(err);
                    }
                })
            },
            function (cb) {
                //create a query in this way so that it finds SP are in defined kms
                let longVal = payload.longitude;
                let latVal = payload.latitude;

                let arr = [longVal, latVal];

                let query = {
                    spLocation: {
                        $near: {
                            $maxDistance: 5 * 1000,
                            $spherical: true,
                            $geometry: {
                                type: 'Point',
                                coordinates: arr
                            }
                        }
                    }
                };
                Services.SpService.getSp(query, {}, {}, function (err, data) {
                    if (err) {
                        cb(err);
                    }
                    else {
                        if (data && data.length > 0) {
                            for (let sp = 0; sp < data.length; sp++) {
                                serviceProviderIds.push(data[sp]._id);
                            }
                            console.log("**************", serviceProviderIds);
                            return cb();
                        }
                        else {
                            console.log('Service Provider not available.');
                            cb();
                        }
                    }
                });
            },
            (cb) => {
                var busySpList = [];
                Services.SpService.getSp({isBusy: true}, {}, {}, function (err, data) {
                    if (err) {
                        cb(err);
                    }
                    else {
                        if (data && data.length > 0) {
                            for (let sp = 0; sp < data.length; sp++) {
                                busySpList.push(data[sp]._id);
                            }
                            console.log("**************", busySpList);

                            //removing  busy Sp's from our booking list SP's

                            for (let i = 0; i < busySpList.length; i++) {
                                for (var j = 0; j < serviceProviderIds.length; j++) {
                                    if (JSON.stringify(busySpList[i]) == JSON.stringify(serviceProviderIds[j])) {
                                        serviceProviderIds.splice(j, 1);
                                        console.log("Updated list of SP", serviceProviderIds);
                                    }
                                }
                            }
                            return cb(null);
                        }
                        else {
                            console.log('All Service Providers are free');
                            return cb(null);
                        }
                    }
                });
            },
            (cb)=> {
                var query = {
                    _id: bookingDetails._id
                };

                var dataToUpdate = {
                    $set: {spList: serviceProviderIds}
                };

                var options = {};

                Services.BookingService.updateBookingModel(query, dataToUpdate, options, function (err, data) {
                    if (err)
                        cb(err);
                    else if (data) {
                        console.log("Booking Details", data);
                        bookingDetails = data;
                        cb(null);
                    }
                });
            }
        ],
        (error, result) => {
            if (error) {
                return callback(error);
            } else {
                return callback(null, bookingDetails);
            }
        });
};


module.exports = {
    createBooking: createBooking
};