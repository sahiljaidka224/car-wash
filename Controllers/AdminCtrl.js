/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const async = require('async');
const mongoose = require('mongoose');
const Models = require('../Models');
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const Services = require('../Services');
const ERROR = Config.AppConstants.STATUS_MSG.ERROR;
const CodeGenerator = require('../Lib/CodeGenerator');
const UploadManager = require('../Lib/UploadManager');


const addGeoLocation = (payload, callback) => {

    let servingLocation;
    async.series([
            (cb) => {
                let query = {
                    location: {
                        $geoIntersects: {
                            $geometry: {
                                type: "Polygon",
                                coordinates: [payload.coordinates]
                            }
                        }
                    }
                };
                Services.GeoLocationServices.getGeoLocationModel(query, {}, {lean: true}, (err, data) => {
                    if (err) return cb(err);
                    if (data.length > 0) {
                        let dataLength = data.length;
                        for (let i = 0; i < dataLength; i++) {
                            if (data[i].IsEnabled) {
                                return cb("POLYGON_INTERSECTS");
                            }
                        }
                    }
                    return cb();
                });
            },
            (cb) => {
                let data = {
                    locationName: [payload.locationName],
                    location: {
                        type: "Polygon",//LineString //Polygon
                        coordinates: [payload.coordinates]
                    }
                };
                Services.GeoLocationServices.createGeoLocationModel(data, function (err, data) {
                    if (err) return cb(err);
                    servingLocation = data;
                    return cb(null);
                });
            }
        ],
        (error, success) => {
            if (error)callback(error);
            else callback(null);

        }
    );
};

const checkInGeoArea = (payload, callback) => {

    let servingLocation;

    async.series([

            (cb) => {
                let longVal = payload.longitude;
                let latVal = payload.latitude;
                let arr = [longVal, latVal];
                let query = {
                    location: {
                        $geoIntersects: {
                            $geometry: {
                                type: "Point",
                                coordinates: arr
                            }
                        }
                    }
                };
                Services.GeoLocationServices.getGeoLocationModel(query, {}, {lean: true}, (err, data) => {
                    if (err) return cb(err);
                    if (data.length > 0) {
                        let dataLength = data.length;
                        for (let i = 0; i < dataLength; i++) {
                            if (data[i].IsEnabled) {
                                return cb();
                            }
                        }
                    }
                    return cb(ERROR.SERVICE_NOT_AVL);
                });
            }
        ],
        (error, success) => {
            if (error)callback(error);
            else callback(null);

        }
    );
};


module.exports = {
    addGeoLocation: addGeoLocation,
    checkInGeoArea: checkInGeoArea
};