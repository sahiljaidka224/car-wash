/**
 * Created by cl-macmini-88 on 8/24/16.
 */
const mongoose = require('mongoose');
const Config = require('../Config');
const Schema = mongoose.Schema;
const booking = Config.AppConstants.DATABASE.BOOKING_STATUS;
const bookingType = Config.AppConstants.DATABASE.BOOKING_TYPE;

const BookingModel = new Schema({
    customerId: {type: Schema.ObjectId, ref: 'CustomerModel'},
    spId: {type: Schema.ObjectId, ref: 'SpModel'},

    startDate: {type: String},
    endDate: {type: String},

    address: {type: String},
    carDetails: {type: String, default: null},

    bookingStatus: {
        type: String, enum: [
            booking.PENDING,
            booking.BOOKED,
            booking.STARTED,
            booking.COMPLETED,
            booking.CANCELLED
        ], default: booking.PENDING
    },

    bookingType: {
        type: String, enum: [
            bookingType.MULTIPLE,
            bookingType.SINGLE_DAY
        ], default: bookingType.SINGLE_DAY
    },

    bookingPrice: {type: Number, default: 0},
    promoCode: {type: String, default: null},
    rebook: {type: Boolean, default: false},

    requestedAt: {type: String, default: null},
    acceptedAt: {type: String, default: null},

    bookingLocation: {
        'type': {type: String, enum: "Point", default: "Point"},
        coordinates: {type: [Number], default: [0, 0]}
    },
    spList: [
        {type: Schema.Types.ObjectId, ref: 'SpModel', default: null}
    ]
});

BookingModel.index({'bookingLocation': "2dsphere"});
module.exports = mongoose.model('BookingModel', BookingModel);