/**
 * Created by cl-macmini-88 on 8/23/16.
 */
'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SpAvailability = new Schema({

    spId: {type: Schema.ObjectId, ref: 'SpModel', required: true, index: true},
    date: {type: Date, required: true},
    slots: [
        {
            time: {type: Number, required: true},
            isAvailable: {type: Boolean}
            //bookingId: {type: Schema.ObjectId, ref: 'booking', index: true}
        }
    ]
});

SpAvailability.index({spId: 1, date: 1, slots: 1}, {unique: true});
module.exports = mongoose.model('SpAvailability', SpAvailability);