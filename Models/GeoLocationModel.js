/**
 * Created by cl-macmini-88 on 9/5/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');

var GeoLocationModel = new Schema({
    locationName: {type: String},
    location: {
        'type': {type: String, enum: "Polygon", default: "Polygon"},
        coordinates: {type: Array}
    },
    IsEnabled: {type: Boolean, default: true}
    //Availability: [{type: Schema.ObjectId, ref: 'Availability'}]
});

GeoLocationModel.index({location: '2dsphere'});
module.exports = mongoose.model('GeoLocationModel', GeoLocationModel);