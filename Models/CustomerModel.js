const mongoose = require('mongoose');
const Config = require('../Config');
const Schema = mongoose.Schema;
const device = Config.AppConstants.DATABASE.DEVICETYPE;
const gender = Config.AppConstants.DATABASE.GENDER;

const CustomerModel = new Schema({
    fullName: {type: String},
    email: {type: String},
    password: {type: String},
    countryCode: {type: String},
    mobileNumber: {type: String},
    deviceToken: {type: String},
    accessToken: {type: String},
    deviceType: {
        type: String, enum: [
            device.ANDROID,
            device.IOS
        ]
    },
    gender: {
        type: String, enum: [
            gender.FEMALE,
            gender.MALE
        ]
    },
    OTPCode: {type: String, trim: true},

    referralCode: {type: String, default: null, trim: true},
    refCodeUsed: {type: String, default: null, trim: true},
    totalCredits: {type: Number, default: 0},
    isRefCodeReg: {type: Boolean, default: false},  //To check if Customer is registered through referral code

    isPhoneVerified: {type: Boolean, default: false},
    registrationDateTime: {type: String, default: null}
});

CustomerModel.index({fullName: "text"});
module.exports = mongoose.model('CustomerModel', CustomerModel);