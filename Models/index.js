module.exports = {
    CustomerModel: require('./CustomerModel'),
    SpModel: require('./SpModel'),
    SpAvailability: require('./SpAvailability'),
    BookingModel: require('./BookingModel'),
    GeoLocationModel: require('./GeoLocationModel')
};