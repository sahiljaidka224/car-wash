/**
 * Created by cl-macmini-88 on 8/23/16.
 */
const mongoose = require('mongoose');
const Config = require('../Config');
const Schema = mongoose.Schema;
const device = Config.AppConstants.DATABASE.DEVICETYPE;


const SpModel = new Schema({
    fullName: {type: String},
    email: {type: String},
    password: {type: String},
    countryCode: {type: String},
    mobileNumber: {type: String},
    deviceToken: {type: String},
    accessToken: {type: String},
    deviceType: {
        type: String, enum: [
            device.ANDROID,
            device.IOS
        ]
    },
    isBlocked: {type: Boolean, default: false},
    adminApproved: {type: Boolean, default: false},
    OTPCode: {type: String, trim: true},
    isPhoneVerified: {type: Boolean, default: false},
    isBusy: {type: Boolean, default: false},
    registrationDateTime: {type: String, default: null},

    spLocation: {
        'type': {type: String, enum: "Point", default: "Point"},
        coordinates: {type: [Number], default: [0, 0]}
    }
});

SpModel.index({'spLocation': "2dsphere"});
module.exports = mongoose.model('SpModel', SpModel);