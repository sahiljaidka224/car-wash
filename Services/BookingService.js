/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

let Models = require('../Models');

let updateBookingModel =  (criteria, dataToSet, options, callback) => {
    options.lean = true;
    options.new = true;
    Models.BookingModel.findOneAndUpdate(criteria, dataToSet, options, callback);
};
//Insert User in DB
let createBookingModel =  (objToSave, callback) => {
    new Models.BookingModel(objToSave).save(callback)
};
//Delete User in DB
let deleteBookingModel =  (criteria, callback) => {
    Models.BookingModel.remove(criteria, callback);
};

//Get Users from DB
let getBookingModel =  (criteria, projection, options, callback) => {
    options.lean = true;
    Models.BookingModel.find(criteria, projection, options, callback);
};



module.exports = {
    updateBookingModel: updateBookingModel,
    createBookingModel: createBookingModel,
    deleteBookingModel: deleteBookingModel,
    getBookingModel: getBookingModel
};
