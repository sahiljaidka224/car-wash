/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

let Models = require('../Models');

let updateSp =  (criteria, dataToSet, options, callback) => {
    options.lean = true;
    options.new = true;
    Models.SpModel.findOneAndUpdate(criteria, dataToSet, options, callback);
};
//Insert User in DB
let createSp =  (objToSave, callback) => {
    new Models.SpModel(objToSave).save(callback)
};
//Delete User in DB
let deleteSp =  (criteria, callback) => {
    Models.SpModel.remove(criteria, callback);
};

//Get Users from DB
let getSp =  (criteria, projection, options, callback) => {
    options.lean = true;
    Models.SpModel.find(criteria, projection, options, callback);
};

//Get All Generated Codes from DB
let getAllGeneratedCodes =  (callback) => {
    var criteria = {
        OTPCode: {$ne: null}
    };
    var projection = {
        OTPCode: 1
    };
    var options = {
        lean: true
    };
    Models.SpModel.find(criteria, projection, options,  (err, dataAry) => {
        if (err) {
            callback(err)
        } else {
            var generatedCodes = [];
            if (dataAry && dataAry.length > 0) {
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.OTPCode.toString())
                });
            }
            callback(null, generatedCodes);
        }
    })
};

module.exports = {
    updateSp: updateSp,
    createSp: createSp,
    deleteSp: deleteSp,
    getSp: getSp,
    getAllGeneratedCodes: getAllGeneratedCodes
};
