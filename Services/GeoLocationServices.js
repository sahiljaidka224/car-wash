/**
 * Created by cl-macmini-88 on 9/5/16.
 */
'use strict';

let Models = require('../Models');

let updateGeoLocationModel = (criteria, dataToSet, options, callback) => {
    options.lean = true;
    options.new = true;
    Models.GeoLocationModel.findOneAndUpdate(criteria, dataToSet, options, callback);
};
//Insert User in DB
let createGeoLocationModel = (objToSave, callback) => {
    new Models.GeoLocationModel(objToSave).save(callback)
};
//Delete User in DB
let deleteGeoLocationModel = (criteria, callback) => {
    Models.GeoLocationModel.remove(criteria, callback);
};

//Get Users from DB
let getGeoLocationModel = (criteria, projection, options, callback) => {
    options.lean = true;
    Models.GeoLocationModel.find(criteria, projection, options, callback);
};


module.exports = {
    updateGeoLocationModel: updateGeoLocationModel,
    createGeoLocationModel: createGeoLocationModel,
    deleteGeoLocationModel: deleteGeoLocationModel,
    getGeoLocationModel: getGeoLocationModel
};
