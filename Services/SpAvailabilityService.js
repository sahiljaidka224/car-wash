/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

let Models = require('../Models');

let updateSpAvailability =  (criteria, dataToSet, options, callback) => {
    options.lean = true;
    options.new = true;
    Models.SpAvailability.update(criteria, dataToSet, options, callback);
};
//Insert User in DB
let createSpAvailability =  (objToSave, callback) => {
    new Models.SpAvailability(objToSave).save(callback)
};
//Delete User in DB
let deleteSpAvailability =  (criteria, callback) => {
    Models.SpAvailability.remove(criteria, callback);
};

//Get Users from DB
let getSpAvailability =  (criteria, projection, options, callback) => {
    options.lean = true;
    Models.SpAvailability.find(criteria, projection, options, callback);
};



module.exports = {
    updateSpAvailability: updateSpAvailability,
    createSpAvailability: createSpAvailability,
    deleteSpAvailability: deleteSpAvailability,
    getSpAvailability: getSpAvailability
};
