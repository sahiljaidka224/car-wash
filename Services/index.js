/**
 * Created by SONY on 3/24/2016.
 */
module.exports = {
    CustomerService: require('./CustomerService'),
    SpService: require('./SpService'),
    SpAvailabilityService: require('./SpAvailabilityService'),
    BookingService: require('./BookingService'),
    GeoLocationServices: require('./GeoLocationServices')
};